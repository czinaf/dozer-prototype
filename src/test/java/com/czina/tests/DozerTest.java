package com.czina.tests;

/**
 * Created by Ferenc on 2015.06.04..
 */

import com.czina.Main;
import com.czina.Strategy;
import com.czina.dao.AddressDao;
import com.czina.dozer.AddressCopier;
import com.czina.dozer.AddressCopierStrategy;
import com.czina.model.Address;
import com.czina.model.AddressVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Main.class)
public class DozerTest {

    @Mock
    private AddressDao addressDao;

    @InjectMocks
    private AddressCopier addressCopier;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void dozer() {
        Address testAddress = new Address();
        testAddress.setZip("zip");
        Mockito.when(addressDao.findById("test_id")).thenReturn(testAddress);

        AddressVO testVO = new AddressVO();
        testVO.setId("test_id");

        Assert.assertEquals("zip", addressCopier.copy(Collections.<Strategy>singleton(AddressCopierStrategy.FROM_DB_BY_ID), testVO).getZip());
    }


}
