package com.czina;

import java.util.Set;

/**
 * Created by Ferenc on 2015.06.04..
 */
public interface Copier<A, B> {

    B copy(Set<Strategy> strategies, A a, B b);

    B copy(Set<Strategy> strategies, A a);
}
