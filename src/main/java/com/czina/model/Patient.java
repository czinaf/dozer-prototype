package com.czina.model;

import java.util.Date;

/**
 * Created by Ferenc on 2015.06.04..
 */
public class Patient {

    private String firstName;
    private String lastName;
    private Date birthDate;
    private Address address;
    private Address mailaddress;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getMailaddress() {
        return mailaddress;
    }

    public void setMailaddress(Address mailaddress) {
        this.mailaddress = mailaddress;
    }
}
