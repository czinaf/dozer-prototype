package com.czina.model;

import java.util.Date;

/**
 * Created by Ferenc on 2015.06.04..
 */
public class PatientVO {

    private String firstName;
    private String lastName;
    private Date birthDate;
    private AddressVO address;
    private AddressVO mailaddress;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public AddressVO getAddress() {
        return address;
    }

    public void setAddress(AddressVO address) {
        this.address = address;
    }

    public AddressVO getMailaddress() {
        return mailaddress;
    }

    public void setMailaddress(AddressVO mailaddress) {
        this.mailaddress = mailaddress;
    }
}
