package com.czina.dozer;

import com.czina.Strategy;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Ferenc on 2015.06.13..
 */
public enum PatientCopierStrategy implements Strategy {

    INCLUDE_ADDRESS,
    EXCLUDE_BIRTH_DATE,
    ADDRESS_FROM_DB_BY_ID,
    MAILADDRESS_FROM_DB_BY_ID;


    public static Set<Strategy> createForPatientList() {
        Set<Strategy> result = new HashSet<Strategy>();
        result.add(EXCLUDE_BIRTH_DATE);
        result.add(AddressCopierStrategy.EXCLUDE_ZIP);
        return result;
    }
}
