package com.czina.dozer;

import com.czina.Copier;
import com.czina.Strategy;
import com.czina.model.Patient;
import com.czina.model.PatientVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;

/**
 * Created by Ferenc on 2015.06.04..
 */
@Service
public class PatientCopier implements Copier<PatientVO, Patient> {

    @Autowired
    private AddressCopier addressCopier;

    public Patient copy(Set<Strategy> strategies, PatientVO from, Patient to) {

        to.setLastName(from.getLastName());
        to.setFirstName(from.getFirstName());
        if (!strategies.contains(PatientCopierStrategy.EXCLUDE_BIRTH_DATE)) {
            to.setBirthDate(from.getBirthDate());
        }

        if (strategies.contains(PatientCopierStrategy.INCLUDE_ADDRESS)) {
            to.setAddress(addressCopier.copy(strategies, from.getAddress()));
        }
        if (strategies.contains(PatientCopierStrategy.ADDRESS_FROM_DB_BY_ID)) {
            to.setAddress(addressCopier.copyById(strategies, from.getAddress()));
        }

        if (strategies.contains(PatientCopierStrategy.MAILADDRESS_FROM_DB_BY_ID)) {
            to.setMailaddress(addressCopier.copyById(strategies, from.getMailaddress()));
        }

        return to;
    }

    public Patient copy(Set<Strategy> strategies, PatientVO patient) {
        return copy(strategies, patient, new Patient());
    }
}
