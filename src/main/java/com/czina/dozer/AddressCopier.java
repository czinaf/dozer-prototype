package com.czina.dozer;

import com.czina.Copier;
import com.czina.Strategy;
import com.czina.dao.AddressDao;
import com.czina.model.Address;
import com.czina.model.AddressVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Ferenc on 2015.06.04..
 */
@Service
public class AddressCopier implements Copier<AddressVO, Address> {

    @Autowired
    private AddressDao addressDao;

    private Set<Strategy> strategies;

    public Address copy(Set<Strategy> strategies, AddressVO from, Address to) {
        to.setCountry(from.getCountry());
        to.setStreet(from.getStreet());
        if (!strategies.contains(AddressCopierStrategy.EXCLUDE_ZIP)) {
            to.setZip(from.getZip());
        }
        return to;
    }

    public Address copy(Set<Strategy> strategies, AddressVO address) {
        return copy(strategies, address, new Address());
    }


}
